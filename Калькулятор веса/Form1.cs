﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Калькулятор_веса
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Ves_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }

        private void Rost_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }

        private void Vozrast_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            rost.Clear();
            ves.Clear();
            vozrast.Clear();
            res.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            res.Clear();

            double Ves, Rost, Vozrast, ketle=0, IVkuper=0, koef=0, IVKreff=0, VRkoef=0, IVVRk=0, IVlorenc=0, kal=0;


            if ((ves.Text == string.Empty) || (rost.Text == string.Empty) || (vozrast.Text == string.Empty) || (radioButton3.Checked == false) && (radioButton4.Checked == false) && (radioButton5.Checked == false) || (comboBox1.Text == ""))
                return;


            Ves = Convert.ToDouble(ves.Text);
            Rost = Convert.ToDouble(rost.Text);
            Vozrast = Convert.ToDouble(vozrast.Text);

            //Тип массы тела
            ketle = Ves / ((Rost / 100) * (Rost / 100));
            if (ketle < 18.5)
                res.Text += "У вас дефицит массы тела (по формуле Кетле)" + Environment.NewLine + Environment.NewLine;
            else if (ketle >= 18.5 && ketle < 24.9)
                res.Text += "У вас нормальная масса тела (по формуле Кетле)" + Environment.NewLine + Environment.NewLine;
            else if (ketle >= 24.9 && ketle < 29.9)
                res.Text += "У вас избыточная масса тела (по формуле Кетле)" + Environment.NewLine + Environment.NewLine;
            else if (ketle >= 29.9 && ketle < 34.9)
                res.Text += "У вас ожирение 1-ой степени (по формуле Кетле)" + Environment.NewLine + Environment.NewLine;
            else if (ketle >= 34.9 && ketle < 39.9)
                res.Text += "У вас ожирение 2-ой степени (по формуле Кетле)" + Environment.NewLine + Environment.NewLine;
            else if (ketle >= 40)
                res.Text += "У вас ожирение 3-ей степени (по формуле Кетле)" + Environment.NewLine + Environment.NewLine;

            //Тип телосложения
            if (radioButton3.Checked)
            {
                koef = 0.9;
                res.Text += "Ваш тип - астенический" + Environment.NewLine + Environment.NewLine;
            }
            else if (radioButton4.Checked)
            {
                koef = 1;
                res.Text += "Ваш тип - нормостенический" + Environment.NewLine + Environment.NewLine;
            }
            else if (radioButton5.Checked)
            {
                koef = 1.1;
                res.Text += "Ваш тип - гиперстенический" + Environment.NewLine + Environment.NewLine;
            }           

            //По формуле Креффа
            if (Rost < 155)
            {
                IVKreff = (Rost - 95 + (Vozrast / 10)) * 0.9 * koef;
                res.Text += $"Ваш идеальный вес по формуле Креффа: {IVKreff:F1} кг" + Environment.NewLine + Environment.NewLine;
            }
            else if (Rost < 175 && Rost >= 155)
            {
                IVKreff = (Rost - 100 + (Vozrast / 10)) * 0.9 * koef;
                res.Text += $"Ваш идеальный вес по формуле Креффа: {IVKreff:F1} кг" + Environment.NewLine + Environment.NewLine;
            }
            else if (Rost >= 175)
            {
                IVKreff = (Rost - 110 + (Vozrast / 10)) * 0.9 * koef;
                res.Text += $"Ваш идеальный вес по формуле Креффа: {IVKreff:F1} кг" + Environment.NewLine + Environment.NewLine;
            }

            //По формуле Купера
            if (man.Checked)
            {
                IVkuper = (Rost * 3.5 / 2.54 - 108) * 0.453;
                res.Text += $"Ваш идеальный вес по формуле Купера: {IVkuper:F1} кг" + Environment.NewLine + Environment.NewLine;
            }
            else if (woman.Checked)
            {
                IVkuper = (Rost * 4.0 / 2.54 - 128) * 0.453;
                res.Text += $"Ваш идеальный вес по формуле Купера: {IVkuper:F1} кг" + Environment.NewLine + Environment.NewLine;
            }

            //Весо-ростовой коэффициент
            if (woman.Checked && radioButton3.Checked && Vozrast >= 15 && Vozrast <= 18)
                VRkoef = 315;
            else if (woman.Checked && radioButton4.Checked && Vozrast >= 15 && Vozrast <= 18)
                VRkoef = 325;
            else if (woman.Checked && radioButton5.Checked && Vozrast >= 15 && Vozrast <= 18)
                VRkoef = 355;
            else if (woman.Checked && radioButton3.Checked && Vozrast > 18 && Vozrast <= 25)
                VRkoef = 325;
            else if (woman.Checked && radioButton4.Checked && Vozrast > 18 && Vozrast <= 25)
                VRkoef = 345;
            else if (woman.Checked && radioButton5.Checked && Vozrast > 18 && Vozrast <= 25)
                VRkoef = 370;
            else if (woman.Checked && radioButton3.Checked && Vozrast > 25 && Vozrast <= 40)
                VRkoef = 335;
            else if (woman.Checked && radioButton4.Checked && Vozrast > 25 && Vozrast <= 40)
                VRkoef = 360;
            else if (woman.Checked && radioButton5.Checked && Vozrast > 25 && Vozrast <= 40)
                VRkoef = 380;

            //Вес по весо-ростовому коэффициенту
            if (woman.Checked && Vozrast >= 15 && Vozrast <= 40)
            {
                IVVRk = (Rost * VRkoef) / 1000;
                res.Text += $"Ваш идеальный вес по весо-ростовому коэффициенту: {IVVRk:F1} кг" + Environment.NewLine + Environment.NewLine;
            }

            //По формуле Лоренца
            if (Vozrast >= 18 && Rost >= 140 && Rost <= 220 && woman.Checked)
            {
                IVlorenc = Rost - 100 - ((Rost - 150) / 2);
                res.Text += $"Ваш идеальный вес по формуле Лоренца: {IVlorenc:F1} кг" + Environment.NewLine + Environment.NewLine;
            }
            else if (Vozrast >= 18 && Rost >= 140 && Rost <= 220 && man.Checked)
            {
                IVlorenc = Rost - 100 - ((Rost - 150) / 4);
                res.Text += $"Ваш идеальный вес по формуле Лоренца: {IVlorenc:F1} кг" + Environment.NewLine + Environment.NewLine;
            }

            //Рассчёт калорий
            if (woman.Checked && comboBox1.Text == "Малоподвижный образ жизни")
            {
                kal = (655 + (9.6 * Ves) + (1.8 * Rost) - 4.7 * Vozrast) * 1.2;
                res.Text += $"Нужное вас количество калорий по формуле Тома Венуто: {kal:F0} ккал" + Environment.NewLine + Environment.NewLine;
            }
            else if (man.Checked && comboBox1.Text == "Малоподвижный образ жизни")
            {
                kal = (66 + (13.7 * Ves) + (5 * Rost) - 6.8 * Vozrast) * 1.2;
                res.Text += $"Нужное вам количество калорий по формуле Тома Венуто: {kal:F0} ккал" + Environment.NewLine + Environment.NewLine;
            }
            else if (woman.Checked && comboBox1.Text == "Тренировки 1-3 раза в неделю")
            {
                kal = (655 + (9.6 * Ves) + (1.8 * Rost) - 4.7 * Vozrast) * 1.38;
                res.Text += $"Нужное вам количество калорий по формуле Тома Венуто: {kal:F0} ккал" + Environment.NewLine + Environment.NewLine;
            }
            else if (man.Checked && comboBox1.Text == "Тренировки 1-3 раза в неделю")
            {
                kal = (66 + (13.7 * Ves) + (5 * Rost) - 6.8 * Vozrast) * 1.38;
                res.Text += $"Нужное вам количество калорий по формуле Тома Венуто: {kal:F0} ккал" + Environment.NewLine + Environment.NewLine;
            }
            else if (woman.Checked && comboBox1.Text == "Тренировки 3-5 раз в неделю")
            {
                kal = (655 + (9.6 * Ves) + (1.8 * Rost) - 4.7 * Vozrast) * 1.56;
                res.Text += $"Нужное вам количество калорий по формуле Тома Венуто: {kal:F0} ккал" + Environment.NewLine + Environment.NewLine;
            }
            else if (man.Checked && comboBox1.Text == "Тренировки 3-5 раз в неделю")
            {
                kal = (66 + (13.7 * Ves) + (5 * Rost) - 6.8 * Vozrast) * 1.56;
                res.Text += $"Нужное вам количество калорий по формуле Тома Венуто: {kal:F0} ккал" + Environment.NewLine + Environment.NewLine;
            }
            else if (woman.Checked && comboBox1.Text == "Ежедневные физич. нагрузки")
            {
                kal = (655 + (9.6 * Ves) + (1.8 * Rost) - 4.7 * Vozrast) * 1.73;
                res.Text += $"Нужное вам количество калорий по формуле Тома Венуто: {kal:F0} ккал" + Environment.NewLine + Environment.NewLine;
            }
            else if (man.Checked && comboBox1.Text == "Ежедневные физич. нагрузки")
            {
                kal = (66 + (13.7 * Ves) + (5 * Rost) - 6.8 * Vozrast) * 1.73;
                res.Text += $"Нужное вам количество калорий по формуле Тома Венуто: {kal:F0} ккал" + Environment.NewLine + Environment.NewLine;
            }
            else if (woman.Checked && comboBox1.Text == "Интенс. ежедневные физ. нагузки")
            {
                kal = (655 + (9.6 * Ves) + (1.8 * Rost) - 4.7 * Vozrast) * 1.95;
                res.Text += $"Нужное вам количество калорий по формуле Тома Венуто: {kal:F0} ккал" + Environment.NewLine + Environment.NewLine;
            }
            else if (man.Checked && comboBox1.Text == "Интенс. ежедневные физ. нагузки")
            {
                kal = (66 + (13.7 * Ves) + (5 * Rost) - 6.8 * Vozrast) * 1.95;
                res.Text += $"Нужное вам количество калорий по формуле Тома Венуто: {kal:F0} ккал" + Environment.NewLine + Environment.NewLine;
            }

            //Расход калорий органами
            res.Text += $"Расход калорий мышцами: {kal*0.26:F0} ккал" + Environment.NewLine + $"Расход калорий печенью: {kal*0.26:F0} ккал" + Environment.NewLine + $"Расход калорий головным мозгом: {kal*0.18:F0} ккал" + Environment.NewLine + $"Расход калорий сердцем: {kal*0.09:F0} ккал" + Environment.NewLine + $"Расход калорий почками: {kal*0.07:F0} ккал" + Environment.NewLine + $"Расход калорий остальными органами: {kal*0.14:F0} ккал";


        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
